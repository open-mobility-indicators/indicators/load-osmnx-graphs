# load-osmnx-graphs notebook python de pré-chargement des graphes de voirie OSM au format graphML 
# en vue de leur utilisation pour l'analyse des voies et des ilots
Projet forké du template Jupyter notebook OpenMobilityIndicators

this notebook reads an OSM .pbf file and creates a networkx/osmnx .graphml out of it
the graph is projected by default in the 4326 CRS, it should be reprojected by the user if needed
in parameters (in the .json file)
 "pbf_url": URL string of the pbf file to download (done by download.sh, not here)
 "pbf_file": pbf file name to download
 "mode" : "walking", "driving", "cycling", as in the osmnx get_network method
 "undirected" : True if the returned graph shall be undirected, False in the graph should be directed / bidrectional

En sortie  : le fichier graphml

## Local install

Preferably use a virtual env:

```bash
python -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt
pip install -r requirements-dev.txt
```

Run notebook:

```bash
jupyter-lab notebook.ipynb
```

## Using Docker

Common env variables

```bash
DOCKER_IMAGE_NAME=jupyter-notebook:latest
DOWNLOAD_DIR=/path/to/download
COMPUTE_DIR=/path/to/compute
PROFILE_ENV_FILE=parameter_profiles/indre.env
```

Build container image

```bash
docker build -t $DOCKER_IMAGE_NAME .
```

Run download:

```bash
docker run -v $DOWNLOAD_DIR:/data/download \
       $DOCKER_IMAGE_NAME --env-file $PROFILE_ENV_FILE /app/download.sh /data/download
```

Run compute:

```bash
docker run -v $DOWNLOAD_DIR:/data/download -v $COMPUTE_DIR:/data/compute \
       $DOCKER_IMAGE_NAME --env-file $PROFILE_ENV_FILE /app/compute.sh /data/download /data/compute
```
